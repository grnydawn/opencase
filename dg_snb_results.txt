
[yslogin1:~/trepo/study/opencase/test/basic/paperwork/results] $ make scatter
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98303.0, "min": 17.0}, "caseperf": {"best": 51.576}}
ctree/031315-0/perf.log.1 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98303.0, "min": 445.0}, "caseperf": {"failure": {"count_4": 247, "count_3": 221}, "success": {"count": 232, "count_betterthan_ref": 61, "top10mean": 42.230999999999995, "count_top10": 10, "mean": 21.636396551724147}}}
ctree/031315-0/perf.log.0 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98253.0, "min": 8227.0}, "caseperf": {"failure": {"count_4": 242, "count_3": 199}, "success": {"count": 259, "count_betterthan_ref": 34, "top10mean": 44.513, "count_top10": 10, "mean": 20.843297297297305}}}
ctree/031315-0/perf.log.2 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98303.0, "min": 1433.0}, "caseperf": {"failure": {"count_4": 239, "count_3": 189}, "success": {"count": 272, "count_betterthan_ref": 87, "top10mean": 43.730999999999995, "count_top10": 10, "mean": 23.146279411764702}}}
ctree/031315-0/perf.log.3 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98303.0, "min": 6188.0}, "caseperf": {"failure": {"count_4": 241, "count_3": 237}, "success": {"count": 222, "count_betterthan_ref": 40, "top10mean": 41.0028, "count_top10": 10, "mean": 21.42533333333333}}}
ctree/031315-0/perf.log.5 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98277.0, "min": 7034.0}, "caseperf": {"failure": {"count_4": 253, "count_3": 237}, "success": {"count": 210, "count_betterthan_ref": 46, "top10mean": 39.9668, "count_top10": 10, "mean": 21.468828571428553}}}
ctree/031315-0/perf.log.4 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98273.0, "min": 5504.0}, "caseperf": {"failure": {"count_4": 219, "count_3": 215}, "success": {"count": 266, "count_betterthan_ref": 71, "top10mean": 42.9456, "count_top10": 10, "mean": 21.00162406015039}}}

rand/031315-0/perf.log.2 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 97859.0, "min": 280.0}, "caseperf": {"failure": {"count_4": 155, "count_3": 459}, "success": {"count": 86, "count_betterthan_ref": 6, "top10mean": 33.807, "count_top10": 10, "mean": 14.812534883720938}}}
rand/031315-0/perf.log.3 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98283.0, "min": 78.0}, "caseperf": {"failure": {"count_4": 155, "count_3": 446}, "success": {"count": 99, "count_betterthan_ref": 3, "top10mean": 30.7138, "count_top10": 10, "mean": 15.219959595959597}}}
rand/031315-0/perf.log.0 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98292.0, "min": 17.0}, "caseperf": {"failure": {"count_4": 173, "count_3": 439}, "success": {"count": 88, "count_betterthan_ref": 5, "top10mean": 32.458800000000004, "count_top10": 10, "mean": 13.52184090909091}}}
rand/031315-0/perf.log.1 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98161.0, "min": 139.0}, "caseperf": {"failure": {"count_4": 144, "count_3": 444}, "success": {"count": 112, "count_betterthan_ref": 13, "top10mean": 35.285599999999995, "count_top10": 10, "mean": 16.104017857142868}}}
rand/031315-0/perf.log.4 ******************************
{"caseord": {"max": 700.0, "min": 1.0}, "casenum": {"max": 98171.0, "min": 31.0}, "caseperf": {"failure": {"count_4": 138, "count_3": 455}, "success": {"count": 107, "count_betterthan_ref": 8, "top10mean": 37.196999999999996, "count_top10": 10, "mean": 16.377476635514014}}}
#--figure "figsize=(30, 18)" \
		#-p "plot, linewidth=2.0" \

number of cases generated correct answers in CTREE
232, 259, 272, 222, 210, 266

number of cases better than reference cases in CTREE
61, 34, 87, 40, 46, 71

number of cases generated correct answers in RAND
86, 99, 88, 112, 107

number of cases better than reference cases in RAND
6, 3, 5, 13, 8
